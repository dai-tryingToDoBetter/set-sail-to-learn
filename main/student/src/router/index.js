import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '@/layout'

Vue.use(VueRouter)

const routes = [
  {
    // 首页路由
    path: '/',
    name: 'layout',
    component: Layout,
    redirect: '/index',
    children: [
      {
        // 首页子路由，用于tab栏切换到首页
        path: 'index',
        component: () => import('@/views/dasboard'),
        name: 'Dashboard',
        meta: { title: '首页' }
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
